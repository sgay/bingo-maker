all: generator
	./generator > content.tex
	pdflatex output.tex 2> /dev/null

generator: generator.cpp
	g++ $< -o $@