#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

map<pair<int,int>, int> dp_binom;

int binom(int n, int p){
	if(p == 0)
		return 1;
	if(p < 0 || p > n)
		return 0;
	if(n == 0)
		return 1;
	if(n < 0)
		return 0;

	auto idx = make_pair(n,p);
	if(dp_binom.find(idx) == dp_binom.end())
		dp_binom[idx] = binom(n, p-1) + binom(n-1, p-1);
	return dp_binom[idx];
}

bool replace_first(string &str, const string &old_s, const string &new_s){
	int idx = str.find(old_s);
	if(idx == string::npos)
		return false;
	str = str.substr(0, idx) + new_s + str.substr(idx+old_s.size());
	return true;
}

int32_t main(){
	int nb_mots;
	int nb_grilles;

	cerr << "BINGO MAKER" << endl;
	cerr << "-----------" << endl;
	cerr << "Nombre de mots ou expressions ?" << endl;
	cin >> nb_mots;
	cerr << "Nombre de grilles ?" << endl;
	cin >> nb_grilles;

	/*if(binom(nb_mots, 16) < nb_grilles){
		cerr << "Pas assez de mots (" << nb_mots << ") pour avoir " << nb_grilles
		     << " grilles avec des ensemble de mots différents. Arrêt." << endl;
		return 1;
	}*/

	vector<string> mots;
	char buf[256];
	cin >> ws;
	for(int i=0; i < nb_mots; ++i){
		cerr << "Mot ou expression (1 ligne) : ";
		cin.getline(buf, 256);
		mots.push_back(buf);
	}

	string tpl;
	if (FILE *fp = fopen("template.tex", "r"))
	{
		char buf[1024];
		while (size_t len = fread(buf, 1, sizeof(buf), fp))
			tpl.insert(tpl.end(), buf, buf + len);
		fclose(fp);
	}

	string hexa = "0123456789ABCDEF";

	for(int i_grille=0; i_grille < nb_grilles; ++i_grille){
		random_shuffle(mots.begin(), mots.end());
		
		string tex = tpl;
		for(int i_case=0; i_case < 16; ++i_case){
			replace_first(tex, "cell"+string(1,hexa[i_case]), mots[i_case]);
		}
		cout << tex << endl;
	}

	return 0;
}