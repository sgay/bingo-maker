{ pkgs ? import ./nix {}, words ? ./sample-input.txt}:
pkgs.stdenv.mkDerivation {
  name = "bingo-de-rentree.pdf";
  src = ./.;
  buildInputs = [
    # g++ is included by stdenv
    pkgs.texlive.combined.scheme-small
  ];
  buildPhase = ''
    make < ${words}
  '';
  installPhase = ''
    cp output.pdf $out
  '';
}
