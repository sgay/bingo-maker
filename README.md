# Bingo Maker

This project aims to automatically generate random 4x4 bingo grids using a set of possible lines of text to put into the grid.

In the current version, it generates random grids without any duplicate protection.

## Dependencies

- make
- g++
- pdflatex

## Customization

`output.tex` and `template.tex` can be easily customized.

## Instructions

`make` compiles and runs the programm, which prompts for:
- the number of lines of text that may be put in a cell of a grid
- the number of grids to generate
- the aforementionned lines
The output is generated in `output.pdf`.

If you are familiar to nix, you can use the provided `default.nix`

## Sample

A sample input is provided in `sample-input.txt`. You may run `make < sample-input.txt`.

*For nix users:* `nix-build` should also build the bingo
